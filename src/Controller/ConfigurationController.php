<?php

namespace Drupal\mailer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mailer\Plugin\MailerMailPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the Mailer configuration page.
 */
class ConfigurationController extends ControllerBase {

  /**
   * The mailer plugin manager.
   *
   * @var \Drupal\mailer\Plugin\MailerMailPluginManagerInterface
   */
  protected $mailer;

  /**
   * Constructs a new ConfigurationController object.
   */
  public function __construct(MailerMailPluginManagerInterface $mailer) {
    $this->mailer = $mailer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mailer_mail')
    );
  }

  /**
   * Render.
   *
   * @return array
   *   Returns render array for this controller.
   */
  public function render() {
    $definitions = $this->mailer->getDefinitions();

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => 'Defined MailerMail plugins',
      '#items' => array_column($definitions, 'id'),
      '#wrapper_attributes' => ['class' => 'container'],
    ];
  }

}
