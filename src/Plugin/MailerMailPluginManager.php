<?php

namespace Drupal\mailer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Mailer template plugin plugin manager.
 */
class MailerMailPluginManager extends DefaultPluginManager implements MailerMailPluginManagerInterface {

  /**
   * Constructs a new MailerTemplatePluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/MailerMail',
      $namespaces,
      $module_handler,
      'Drupal\mailer\Plugin\MailerMailPluginInterface',
      'Drupal\mailer\Annotation\MailerMail'
    );

    $this->alterInfo('mailer_mail_info');
    $this->setCacheBackend($cache_backend, 'mailer_mail_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public static function getTemplateKey($plugin_id, $template) {
    $template = str_replace('-', '_', $template);
    return $plugin_id . '_' . $template;
  }

  /**
   * Get the default config for this plugin instance.
   *
   * @param int $plugin_id
   *   The plugin id to load the config for.
   *
   * @return \Drupal\mailer\Plugin\MailerMailConfigInterface
   *   the mailer mail config object loaded with default values.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getDefaultConfig($plugin_id) {
    $mailplugin = $this->createInstance($plugin_id);
    return $mailplugin->getDefaultConfig();
  }

}
