<?php

namespace Drupal\mailer\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Mailer template plugin plugins.
 */
interface MailerMailPluginInterface extends PluginInspectionInterface {

  /**
   * Get the email message.
   *
   * @return string
   *   The body of the email. Mostly a rendered tpl file.
   */
  public function getMessage();

  /**
   * Get the mail key to be used by hook_mail.
   *
   * @return string
   *   Mail key.
   */
  public function getMailKey();

  /**
   * Return the mailermail config object, return defaultconfig if not set.
   *
   * @return \Drupal\mailer\Plugin\MailerMailConfigInterface
   *   MailerMailConfig Object.
   */
  public function getConfig();

  /**
   * Return the template file to use for this email.
   *
   * Override this function if a dynamic template should be picked.
   */
  public function getTemplate();

  /**
   * Send out the email.
   *
   * @return bool
   *   Boolean value if sending was successful.
   */
  public function send();

}
