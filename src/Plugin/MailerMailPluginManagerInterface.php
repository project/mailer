<?php

namespace Drupal\mailer\Plugin;

/**
 * Provides the Mailer template plugin plugin manager.
 *
 * @package Drupal\mailer\Plugin
 */
interface MailerMailPluginManagerInterface {

  /**
   * Grab the key to use as the Drupal template key.
   */
  public static function getTemplateKey($key, $template);

}
