<?php

namespace Drupal\mailer\Plugin;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\mailer\Plugin\Factory\MailerMailConfigFactory;
use Drupal\mailer_storage\Entity\MailerStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Mailer template plugin plugins.
 */
abstract class MailerMailPluginBase extends PluginBase implements MailerMailPluginInterface, ContainerFactoryPluginInterface, DiscoveryInterface {

  /**
   * Drupal core config object for logger.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configService;

  /**
   * Drupal core mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Drupal core renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal core logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The storage bundle for mailer_storage.
   *
   * @var string
   */
  protected $storageBundle = 'default';

  /**
   * The mailermail config factory.
   *
   * @var \Drupal\mailer\Plugin\Factory\MailerMailConfigFactory
   */
  protected $configFactory;

  /**
   * MailerMailConfig object for this plugin instance.
   *
   * @var \Drupal\mailer\Plugin\MailerMailConfigInterface
   */
  protected $config;

  /**
   * The MailerMailManager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $manager;

  /**
   * TranslationManager class.
   *
   * Used to set the language for string translation while rendering.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $translationManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MailerMailPluginBase constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailerMailPluginManagerInterface $manager, ConfigFactoryInterface $configService, MailManagerInterface $mail_manager, RendererInterface $renderer, LoggerChannelFactoryInterface $logger_factory, TranslationManager $translation_manager, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->manager = $manager;
    $this->configService = $configService;
    $this->mailManager = $mail_manager;
    $this->renderer = $renderer;
    $this->logger = $logger_factory->get('mailer');
    $this->translationManager = $translation_manager;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;

    // If configuration contains config object, set config value.
    if (isset($configuration['config']) && $configuration['config'] instanceof MailerMailConfigInterface) {
      $this->config = $configuration['config'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mailer_mail'),
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer'),
      $container->get('logger.factory'),
      $container->get('string_translation'),
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    if (!isset($this->config)) {
      $this->config = $this->getDefaultConfig();
    }
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate() {
    return $this->getPluginDefinition()['templates'][0];
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    $template = $this->getTemplate();
    $split = explode('/', $template);
    $template_name = array_pop($split);
    $key = $this->getPluginDefinition()['id'];

    // Set language for the email.
    $this->translationManager->setDefaultLangcode($this->config->getLangcode());

    // Render the mail.
    $render = [
      '#theme' => $this->manager->getTemplateKey($key, $template_name),
      '#config' => $this->getConfig(),
    ];
    $output = $this->renderer->renderPlain($render);

    // Reset default language.
    $default = $this->languageManager->getDefaultLanguage()->getId();
    $this->translationManager->setDefaultLangcode($default);

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailKey() {
    return 'mailer_mail_' . $this->getPluginDefinition()['id'];
  }

  /**
   * Get or create the config factory for MailerMailConfig objects.
   *
   * @return \Drupal\mailer\Plugin\Factory\MailerMailConfigFactory
   *   The config factory.
   */
  protected function getConfigFactory() {
    if (!$this->configFactory) {
      $this->configFactory = new MailerMailConfigFactory($this, '\Drupal\mailer\Plugin\MailerMailConfigInterface');
    }
    return $this->configFactory;
  }

  /**
   * Get the default mailermail config object for this plugin.
   *
   * @return \Drupal\mailer\Plugin\MailerMailConfigInterface
   *   The config object.
   */
  public function getDefaultConfig() {
    return $this->getConfigFactory()->createInstance($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    return $this->getPluginDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    return [$this->getPluginDefinition()];
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefinition($plugin_id) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getParams() {
    $params = [];
    $params['message'] = $this->getMessage();
    $params['title'] = $this->getConfig()->getSubject();
    $params['from'] = $this->getConfig()->getFrom();

    if (!empty($this->getConfig()->getAttachments())) {
      $params['attachments'] = $this->getConfig()->getAttachments();
    }

    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function send() {
    $key = $this->getMailKey();
    $to = $this->getConfig()->getTo();
    $reply = $this->getConfig()->getReplyEmail();
    $params = $this->getParams();

    $mail_langcode = $this->getConfig()->getLangcode();
    $mail_send = TRUE;

    $mail_result = $this->mailManager->mail('mailer', $key, $to, $mail_langcode, $params, $reply, $mail_send);

    if ($this->moduleHandler->moduleExists('mailer_storage')) {
      $this->saveEmail($to, $params['title'], $params['message'], $params['attachments'] ?? []);
    }

    if ($mail_result['result'] !== TRUE) {
      $this->logger->error('Er is een probleem opgetreden bij het versturen van uw bericht @key.', ['@key' => $this->getPluginDefinition()['id']]);
      return FALSE;
    }
    else {
      $this->logger->info('Bericht @key is verzonden naar @mail', [
        '@key' => $this->getPluginDefinition()['id'],
        '@mail' => $to,
      ]);
      return TRUE;
    }
  }

  /**
   * Save the email to the mailer_storage entity.
   *
   * @param string $to
   *   The email address to send the email to.
   * @param string $subject
   *   The subject of the email.
   * @param string $message
   *   The message of the email.
   * @param array $attachments
   *   The attachments of the email.
   */
  protected function saveEmail($to, $subject, $message, $attachments = []) {
    $mailer_storage = MailerStorage::create([
      'bundle' => $this->storageBundle,
      'label' => 'Sending email with subjectL ' . $subject,
      'to' => $to,
      'subject' => $subject,
      'message' => [
        'value' => $message,
        'format' => 'full_html',
      ],
    ]);
    $mailer_storage->save();
  }

}
