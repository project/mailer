<?php

namespace Drupal\mailer\Plugin;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MailerMailConfigBase.
 *
 * Base class for the config objects used by MailerMail plugins.
 * These classes contain all the data that is send to the twig file for
 * rendering.
 *
 * @package Drupal\mailer\Plugin
 */
class MailerMailConfigBase implements MailerMailConfigInterface {
  use StringTranslationTrait;

  /**
   * Drupal conre language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Receiving email address for the mail.
   *
   * @var string
   */
  protected $to;

  /**
   * Sending email for this mail.
   *
   * Default value will be the site email address.
   *
   * @var string
   */
  protected $from;

  /**
   * The parameter to set as email return address.
   *
   * If this parameter is omitted, the from email will be used by default.
   *
   * @var string
   */
  protected $replyEmail;

  /**
   * Langcode for the email.
   *
   * @var string
   */
  protected $langcode;

  /**
   * The title to be used in the HEAD of the html email.
   *
   * This defaults to the value of subject if not set.
   *
   * @var string
   */
  protected $title;

  /**
   * Subject for the mail.
   *
   * @var string
   */
  protected $subject;

  /**
   * Central message for the email.
   *
   * @var string
   */
  protected $message;

  /**
   * Url to the site logo. This logo is placed in the header of the mail.
   *
   * @var string
   */
  protected $siteLogo;

  /**
   * Url to the front page of this website.
   *
   * @var string
   */
  protected $frontPageUrl;

  /**
   * The urlgenerator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The attachments to send with the email.
   *
   * @var mixed
   */
  protected $attachments;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * MailerMailConfigBase constructor.
   */
  public function __construct(LanguageManagerInterface $language_manager, UrlGeneratorInterface $url_generator, ConfigFactoryInterface $config_factory, ThemeHandlerInterface $theme_handler) {
    $this->languageManager = $language_manager;
    $this->urlGenerator = $url_generator;
    $this->configFactory = $config_factory;
    $this->themeHandler = $theme_handler;

    $this->subject = $this->t('You received a new email from @site',
      [
        '@site' => $this->configFactory->get('system.site')->get('name'),
      ]);

    $site_email = $this->configFactory->get('system.site')->get('mail');
    $this->to = $site_email;
    $this->from = $site_email;
  }

  /**
   * Create function to fill in the constructor.
   *
   * Use this function to create default values.
   */
  public static function create(ContainerInterface $container, array $configuration) {
    return new static(
      $container->get('language_manager'),
      $container->get('url_generator'),
      $container->get('config.factory'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage():string {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getTo() {
    return $this->to;
  }

  /**
   * {@inheritdoc}
   */
  public function getFrom() {
    return $this->from;
  }

  /**
   * {@inheritdoc}
   */
  public function getReplyEmail() {
    return $this->replyEmail ?? $this->getFrom();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title ?? $this->getSubject();
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode() {
    if (empty($this->langcode)) {
      $this->setLangcode();
    }
    return $this->langcode;
  }

  /**
   * Get the mail attachments.
   *
   * @return mixed
   *   The attachments to be send with the email.
   */
  public function getAttachments() {
    return $this->attachments;

  }

  /**
   * Get the site logo.
   *
   * @return string
   *   Url to the site logo.
   */
  public function getSiteLogo() {
    if (empty($this->siteLogo)) {
      $this->setSiteLogo();
    }
    return $this->siteLogo;
  }

  /**
   * Get the url for the front page.
   *
   * @return string
   *   Absolute url to the front page.
   */
  public function getFrontPageUrl() {
    if (empty($this->siteLogo)) {
      $this->frontPageUrl = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    }
    return $this->frontPageUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getToUserLangcode() {
    $user = user_load_by_mail($this->getTo());
    if (($user != NULL) && ($user->getPreferredLangcode() != NULL)) {
      return $user->getPreferredLangcode();
    }
  }

  /**
   * Set the email message.
   *
   * @param string $message
   *   The central text of the email.
   */
  public function setMessage(string $message): void {
    $this->message = $message;
  }

  /**
   * Get the mail subject.
   *
   * @param string $subject
   *   The mail subject.
   */
  public function setSubject($subject): void {
    $this->subject = $subject;
  }

  /**
   * Set receiver of email.
   *
   * @param mixed $to
   *   Receiver email address.
   */
  public function setTo($to) {
    $this->to = $to;
  }

  /**
   * Set title tag for head of email.
   *
   * @param mixed $title
   *   Title tag.
   */
  public function setTitle($title): void {
    $this->title = $title;
  }

  /**
   * Set the from email.
   *
   * @param mixed $email
   *   The from email address.
   */
  public function setFrom($email): void {
    $this->from = $email;
  }

  /**
   * Set the reply email address.
   *
   * @param string $email
   *   The reply email address.
   */
  public function setReplyEmail($email): void {
    $this->replyEmail = $email;
  }

  /**
   * Set langcode for this email.
   *
   * If langcode param is empty, a default langcode will be set.
   *
   * @param mixed $langcode
   *   Langcode.
   */
  public function setLangcode($langcode = NULL): void {
    if (!isset($langcode)) {
      $langcode = $this->getToUserLangcode();
      $this->langcode = $langcode ?? $this->languageManager->getDefaultLanguage()->getId();
    }
    else {
      $this->langcode = $langcode;
    }

  }

  /**
   * Set the site logo.
   *
   * @param string $siteLogo
   *   The logo to use.
   */
  public function setSiteLogo($siteLogo = NULL) {
    if (empty($siteLogo)) {
      $default = $this->themeHandler->getDefault();
      $setting = theme_get_setting('logo.url', $default);

      $this->siteLogo = Url::fromUri(
        'base:' . $setting,
        ['absolute' => TRUE]
      );
    }
    else {
      $this->siteLogo = $siteLogo;
    }
  }

  /**
   * Set the front page url.
   *
   * @param string $frontPageUrl
   *   Url for the front page.
   */
  public function setFrontPageUrl($frontPageUrl) {
    $this->frontPageUrl = $frontPageUrl;
  }

  /**
   * Set the attachments variable.
   *
   * @param mixed $attachments
   *   The attachments to be send with the email.
   */
  public function setAttachments($attachments): void {
    $this->attachments = $attachments;
  }

}
