<?php

namespace Drupal\mailer\Plugin\Factory;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Factory\DefaultFactory;

/**
 * Class MailerMailConfigFactory.
 *
 *  Defines factory for the mailerconfiginterface objects.
 *
 * @package Drupal\mailer\Plugin\Factory
 */
class MailerMailConfigFactory extends DefaultFactory {

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->discovery->getDefinition($plugin_id);
    $config_class = $plugin_definition['config'];
    $configuration['plugin_definition'] = $plugin_definition;

    if (!class_exists($config_class)) {
      throw new PluginException(sprintf('Plugin (%s) config instance class "%s" does not exist.', $plugin_id, $config_class));
    }

    if (!is_subclass_of($config_class, $this->interface)) {
      throw new PluginException(sprintf('The defined config class (%s) must implement interface %s.', $config_class, $this->interface));
    }

    return $config_class::create(\Drupal::getContainer(), $configuration);
  }

}
