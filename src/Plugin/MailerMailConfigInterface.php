<?php

namespace Drupal\mailer\Plugin;

/**
 * Defines an interface for Mailer template plugin plugins.
 */
interface MailerMailConfigInterface {

  /**
   * Get the subject for the email.
   *
   * @return string
   *   The subject.
   */
  public function getSubject();

  /**
   * Get the central email text.
   *
   * @return string
   *   Body of the email.
   */
  public function getMessage();

  /**
   * Get the reply email address.
   *
   * If no repy email is set, this function will return the from email
   * by default.
   *
   * @return string
   *   Email address.
   */
  public function getReplyEmail();

  /**
   * Get the email sender.
   *
   * @return string
   *   Email address.
   */
  public function getFrom();

  /**
   * Get the receiver email address.
   *
   * @return string
   *   Email address.
   */
  public function getTo();

  /**
   * Get title tag for the email if not set, return subject.
   *
   * @return string
   *   Value for the title tag.
   */
  public function getTitle();

  /**
   * Get the langcode for this email.
   *
   * @return string
   *   2 letter langcode, defaults to site language.
   */
  public function getLangcode();

}
