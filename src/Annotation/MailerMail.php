<?php

namespace Drupal\mailer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Mailer template plugin item annotation object.
 *
 * @see \Drupal\mailer\Plugin\MailerTemplatePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class MailerMail extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;


  /**
   * The email template to use.
   *
   * @var string[]
   */
  public $templates = [];

  /**
   * The config class associated with this plugin.
   *
   * @var string
   */
  public $config;

}
