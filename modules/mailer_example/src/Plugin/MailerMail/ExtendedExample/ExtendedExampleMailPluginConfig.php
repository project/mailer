<?php

namespace Drupal\mailer_example\Plugin\MailerMail\ExtendedExample;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\mailer\Plugin\MailerMailConfigBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Mailer template plugin plugin manager.
 */
class ExtendedExampleMailPluginConfig extends MailerMailConfigBase {

  /**
   * The page link value.
   *
   * @var string
   */
  protected $pageLink;

  /**
   * The current time value.
   *
   * @var int
   */
  protected $currentTime;

  /**
   * Constructs a new MailerTemplatePluginManager object.
   */
  public function __construct(LanguageManagerInterface $language_manager, UrlGeneratorInterface $url_generator, ConfigFactoryInterface $config_factory, ThemeHandlerInterface $theme_handler, $current_time) {
    parent::__construct($language_manager, $url_generator, $config_factory, $theme_handler);
    $this->currentTime = $current_time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration) {
    return new static(
      $container->get('language_manager'),
      $container->get('url_generator'),
      $container->get('config.factory'),
      $container->get('theme_handler'),
      time()
    );
  }

  /**
   * Return page link value.
   */
  public function getPageLink() {
    return $this->pageLink;
  }

  /**
   * Get current time value.
   */
  public function getCurrentTime() {
    return $this->currentTime;
  }

  /**
   * Set page link value.
   */
  public function setPageLink($page_link): void {
    $this->pageLink = $page_link;
  }

  /**
   * Set current time value.
   */
  public function setCurrentTime($current_time): void {
    $this->currentTime = $current_time;
  }

}
