<?php

namespace Drupal\mailer_example\Plugin\MailerMail\ExtendedExample;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\mailer\Plugin\MailerMailPluginBase;
use Drupal\mailer\Plugin\MailerMailPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Extended example' mail plugin.
 *
 * @package Drupal\mailer\Plugin\MailerMail
 *
 * @MailerMail (
 *   id = "extended_example",
 *   label = @Translation("Extended example test email"),
 *   templates = {
 *    "mailer_example/src/Plugin/MailerMail/ExtendedExample/ExtendedExample",
 *    "mailer_example/src/Plugin/MailerMail/ExtendedExample/AlternativeTemplate",
 *   },
 *   config = "\Drupal\mailer_example\Plugin\MailerMail\ExtendedExample\ExtendedExampleMailPluginConfig"
 * )
 */
class ExtendedExampleMailPlugin extends MailerMailPluginBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * MailerMailPluginBase constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailerMailPluginManagerInterface $manager, ConfigFactoryInterface $configService, MailManagerInterface $mail_manager, RendererInterface $renderer, LoggerChannelFactoryInterface $logger_factory, AccountProxy $current_user, TranslationManager $translation_manager, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $manager, $configService, $mail_manager, $renderer, $logger_factory, $translation_manager, $language_manager, $module_handler);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mailer_mail'),
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer'),
      $container->get('logger.factory'),
      $container->get('current_user'),
      $container->get('string_translation'),
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }

  /**
   * Multiple template example.
   *
   * This function will select another template for the mail
   * when a user is logged in.
   */
  public function getTemplate() {
    // Send first template to anonymous users, second template
    // to logged-in users.
    if ($this->currentUser->isAnonymous()) {
      return $this->getPluginDefinition()['templates'][0];
    }
    else {
      return $this->getPluginDefinition()['templates'][1];
    }
  }

}
