<?php

namespace Drupal\mailer_example\Plugin\MailerMail\BasicExample;

use Drupal\mailer\Plugin\MailerMailPluginBase;

/**
 * Class VZATestMailPlugin.
 *
 * @package Drupal\mailer\Plugin\MailerMail
 *
 * @MailerMail (
 *   id = "basic_example",
 *   label = @Translation("simple example email"),
 *   templates = {
 *    "mailer/templates/mailermail--base",
 *   },
 *   config = "\Drupal\mailer\Plugin\MailerMailConfigBase"
 * )
 */
class BasicExample extends MailerMailPluginBase {

}
