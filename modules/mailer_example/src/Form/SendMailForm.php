<?php

namespace Drupal\mailer_example\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for sending emails.
 */
class SendMailForm extends FormBase {
  use StringTranslationTrait;

  /**
   * The mailer plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $mailer;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new SendMailForm object.
   */
  public function __construct(
    PluginManagerInterface $mailer,
    DateFormatterInterface $date_formatter,
    MessengerInterface $messenger,
  ) {
    $this->mailer = $mailer;
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mailer_mail'),
      $container->get('date.formatter'),
      $container->get("messenger")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_mail_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['time'] = [
      '#type' => 'value',
      '#value' => $this->dateFormatter->format(time(), 'custom', 'l j F Y -- H:i:s'),
    ];

    $form['mail_selection'] = [
      '#type' => 'radios',
      '#title' => $this->t('Email to send'),
      '#options' => [
        'sendBasicExample' => $this->t('Basic example'),
        'sendExtendedExample' => $this->t('Extended example'),
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send email'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      $this->messenger->addMessage($key . ': ' . $value);
    }

    $mail = $form_state->getValue('mail_selection');
    $this->{$mail}($form_state);

  }

  /**
   * Send out the most basic email plugin possible.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function sendBasicExample($form_state) {
    // Send out email with mailer.
    // 1. Get the mailer plugin instance.
    $mail = $this->mailer->createInstance('basic_example');
    // 2. Assemble the config object for the template file.
    $config = $mail->getConfig();
    $config->setSubject($form_state->getValue('time'));
    $config->setMessage('This email is just a basic example');
    // 3. Send out your email.
    $mail->send();
  }

  /**
   * Send out a more complex example.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function sendExtendedExample($form_state) {
    // Send out email with mailer.
    // 1. Get the mailer plugin instance.
    $mail = $this->mailer->createInstance('extended_example');
    // 2. Assemple the config object for the template file.
    $config = $mail->getConfig();
    $config->setSubject($form_state->getValue('time'));
    // 3. Send out your email.
    $mail->send();
  }

}
