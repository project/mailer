<?php

declare(strict_types=1);

namespace Drupal\mailer_storage;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of mailer storage type entities.
 *
 * @see \Drupal\mailer_storage\Entity\MailerStorageType
 */
final class MailerStorageTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No mailer storage types available. <a href=":link">Add mailer storage type</a>.',
      [':link' => Url::fromRoute('entity.mailer_storage_type.add_form')->toString()],
    );

    return $build;
  }

}
