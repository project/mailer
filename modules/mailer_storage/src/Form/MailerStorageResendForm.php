<?php

declare(strict_types=1);

namespace Drupal\mailer_storage\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailer\Plugin\MailerMailPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the mailer storage entity edit forms.
 */
final class MailerStorageResendForm extends ContentEntityForm {

  /**
   * The mailer plugin manager.
   *
   * @var \Drupal\mailer\Plugin\MailerMailPluginManagerInterface
   */
  protected $mailer;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, MailerMailPluginManagerInterface $mailer) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->mailer = $mailer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.mailer_mail')
    );

  }

  /**
   * Resend the email and save a new mailer storage entity.
   */
  public function save(array $form, FormStateInterface $form_state) {
    $mail = $this->mailer->createInstance('resend_email');
    $config = $mail->getConfig();
    $config->setSubject($form_state->getValue('subject')[0]['value']);
    $config->setMessage($form_state->getValue('message')[0]['value']);
    $config->setTo($form_state->getValue('to')[0]['value']);
    // 3. Send out your email.
    $mail->send();
  }

}
