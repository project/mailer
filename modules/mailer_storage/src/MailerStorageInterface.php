<?php

declare(strict_types=1);

namespace Drupal\mailer_storage;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a mailer storage entity type.
 */
interface MailerStorageInterface extends ContentEntityInterface, EntityOwnerInterface {

}
