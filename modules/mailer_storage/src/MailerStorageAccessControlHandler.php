<?php

declare(strict_types=1);

namespace Drupal\mailer_storage;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the mailer storage entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class MailerStorageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view mailer_storage', 'administer mailer_storage types'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit mailer_storage', 'administer mailer_storage types'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete mailer_storage', 'administer mailer_storage types'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create mailer_storage', 'administer mailer_storage types'], 'OR');
  }

}
