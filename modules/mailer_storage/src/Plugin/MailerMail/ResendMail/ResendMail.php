<?php

namespace Drupal\mailer_storage\Plugin\MailerMail\ResendMail;

use Drupal\mailer\Plugin\MailerMailPluginBase;

/**
 * Class VZATestMailPlugin.
 *
 * @package Drupal\mailer\Plugin\MailerMail
 *
 * @MailerMail (
 *   id = "resend_email",
 *   label = @Translation("Resend a previous email"),
 *   templates = {
 *    "mailer_storage/src/Plugin/MailerMail/ResendMail/ResendMail",
 *   },
 *   config = "\Drupal\mailer\Plugin\MailerMailConfigBase"
 * )
 */
class ResendMail extends MailerMailPluginBase {

}
