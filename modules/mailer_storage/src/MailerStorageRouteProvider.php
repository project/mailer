<?php

namespace Drupal\mailer_storage;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for the Mailer Storage entity.
 */
class MailerStorageRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes($entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($resend_form_route = $this->getResendFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.resend_form", $resend_form_route);
    }

    return $collection;
  }

  /**
   * Gets the resend form route for the mailer_storage entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getResendFormRoute($entity_type) {
    if ($entity_type->hasLinkTemplate('resend-form')) {
      $route = new Route($entity_type->getLinkTemplate('resend-form'));
      $route
        ->setDefaults([
          '_entity_form' => $entity_type->id() . '.resend',
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
        ])
        ->setRequirement('_entity_access', $entity_type->id() . '.update')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ]);

      return $route;
    }
  }

}
