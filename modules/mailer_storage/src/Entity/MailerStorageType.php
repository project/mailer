<?php

declare(strict_types=1);

namespace Drupal\mailer_storage\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Mailer Storage type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "mailer_storage_type",
 *   label = @Translation("Mailer Storage type"),
 *   label_collection = @Translation("Mailer Storage types"),
 *   label_singular = @Translation("mailer storage type"),
 *   label_plural = @Translation("mailer storages types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mailer storages type",
 *     plural = "@count mailer storages types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\mailer_storage\Form\MailerStorageTypeForm",
 *       "edit" = "Drupal\mailer_storage\Form\MailerStorageTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\mailer_storage\MailerStorageTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer mailer_storage types",
 *   bundle_of = "mailer_storage",
 *   config_prefix = "mailer_storage_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/mailer_storage_types/add",
 *     "edit-form" = "/admin/structure/mailer_storage_types/manage/{mailer_storage_type}",
 *     "delete-form" = "/admin/structure/mailer_storage_types/manage/{mailer_storage_type}/delete",
 *     "collection" = "/admin/structure/mailer_storage_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class MailerStorageType extends ConfigEntityBundleBase {

  /**
   * The machine name of this mailer storage type.
   */
  protected string $id;

  /**
   * The human-readable name of the mailer storage type.
   */
  protected string $label;

}
