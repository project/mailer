<?php

declare(strict_types=1);

namespace Drupal\mailer_storage\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mailer_storage\MailerStorageInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the mailer storage entity class.
 *
 * @ContentEntityType(
 *   id = "mailer_storage",
 *   label = @Translation("Mailer Storage"),
 *   label_collection = @Translation("Mailer Storages"),
 *   label_singular = @Translation("mailer storage"),
 *   label_plural = @Translation("mailer storages"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mailer storages",
 *     plural = "@count mailer storages",
 *   ),
 *   bundle_label = @Translation("Mailer Storage type"),
 *   handlers = {
 *      "route_provider" = {
 *        "html" = "Drupal\mailer_storage\MailerStorageRouteProvider",
 *      },
 *     "list_builder" = "Drupal\mailer_storage\MailerStorageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\mailer_storage\MailerStorageAccessControlHandler",
 *     "form" = {
 *       "resend" = "Drupal\mailer_storage\Form\MailerStorageResendForm",
 *       "edit" = "Drupal\mailer_storage\Form\MailerStorageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     }
 *   },
 *   base_table = "mailer_storage",
 *   revision_table = "mailer_storage_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer mailer_storage types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "to" = "to",
 *     "subject" = "subject",
 *     "message" = "message",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/mailer-storage",
 *     "add-form" = "/admin/content/mailer/add/{mailer_storage_type}",
 *     "add-page" = "/admin/content/mailer/add",
 *     "canonical" = "/admin/content/mailer/{mailer_storage}",
 *     "edit-form" = "/admin/content/mailer/{mailer_storage}/edit",
 *     "delete-form" = "/admin/content/mailer/{mailer_storage}/delete",
 *     "resend-form" = "/admin/content/mailer-storage/{mailer_storage}/resend",
 *     "delete-multiple-form" = "/admin/content/mailer-storage/delete-multiple",
 *   },
 *   bundle_entity_type = "mailer_storage_type",
 *   field_ui_base_route = "entity.mailer_storage_type.edit_form",
 * )
 */
final class MailerStorage extends RevisionableContentEntityBase implements MailerStorageInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the mailer storage was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['to'] = BaseFieldDefinition::create('email')
      ->setLabel(t('To'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subject'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
